import csv
import sqlite3
import logging
import os
import sys

# This function initialize a list of Vehicles to insert in BDD
# Parameters : [pathToCSV] is the link to the source document
# Return : [listVehicules] is a list of vehicles
def setVehiclesList(pathToCSV):
    try :
        listeVehicules = []

        if os.path.exists(pathToCSV) and os.path.isfile(pathToCSV):

            with open(pathToCSV, newline='') as fichierCourant:
                contenuFichierCourant = csv.reader(fichierCourant, delimiter=";")
                
                
                for row in contenuFichierCourant:
                    dictionnaireAuto = {
                        'adresse_titulaire' : row[0],
                        'nom' : row[1],
                        'prenom' : row[2], 
                        'immatriculation': row[3], 
                        'date_immatriculation': row[4], 
                        'vin' : row[5], 
                        'marque': row[6], 
                        'denomination_commerciale': row[7], 
                        'couleur': row[8], 
                        'carrosserie': row[9],
                        'categorie': row[10], 
                        'cylindree': row[11], 
                        'energie': row[12], 
                        'places': row[13], 
                        'poids': row[14], 
                        'puissance': row[15],
                        'type': row[16],
                        'variante': row[17],
                        'version': row[18]
                    }
                        
                    listeVehicules.append(dictionnaireAuto)

                print("La liste de véhicule a bien été initialisée !")

                return listeVehicules                    
    
    except Exception as erreur :
        print("Erreur dans la lecture du fichier source ! ")
        sys.exit(1)

# This function print on the screen vehicle's datas
# Parameter : [A list of vehicles]
# Return : [nothing]
def showVehicles(listVehicles):

    try:

        for vehicle in listVehicles[1:]:
            print(vehicle)

    except Exception as erreur:

        print("Impossible d'afficher les données dans la liste !")

# This function counts the number of rows in a table
# Parameter : [myConnectionToBDD]
# Return : [number of rows] is an integer
def rowCount(myConnectionToBDD):

    numberOfRows = 0

    cursor = myConnectionToBDD.cursor()

    cursor.execute("SELECT * FROM siv;")

    numberOfRows = len(cursor.fetchall())

    #cursor = newdb.execute('select * from mydb;')
    #print len(cursor.fetchall())

    return numberOfRows

# This function create the database used by the app
# Parameters : [myConnection]
# Return : [Nothing]
def initTable(myConnectionToBDD):
    myCursor = myConnectionToBDD.cursor()

    myCursor.execute('''
                CREATE TABLE IF NOT EXISTS siv(
                    adress_titulaire VARCHAR(100) NOT NULL,
                    nom VARCHAR(20) NOT NULL,
                    prenom VARCHAR(20) NOT NULL,
                    immatriculation VARCHAR(10) PRIMARY KEY NOT NULL,
                    date_immatriculation VARCHAR(10) NOT NULL, 
                    vin VARCHAR(10) NOT NULL, 
                    marque VARCHAR(20) NOT NULL, 
                    denomination VARCHAR(30) NOT NULL, 
                    couleur VARCHAR(20) NOT NULL, 
                    carrosserie VARCHAR(10) NOT NULL, 
                    categorie VARCHAR(10) NOT NULL,
                    cylindree INTEGER(4) NOT NULL, 
                    energie INTEGER(8) NOT NULL, 
                    places INTEGER(2) NOT NULL, 
                    poids INTEGER(4) NOT NULL, 
                    puissance INTEGER(3) NOT NULL, 
                    typeVehicule VARCHAR(10) NOT NULL, 
                    variante VARCHAR(10) NOT NULL, 
                    versionVehicule VARCHAR(10) NOT NULL );''')

#This function is made to control if the datas we want to insert is already in the database
#Parameters : [{vehicle}, cursor ] is the dictionnary of datas we want to insert
#Return : [bool] -> TRUE if data is already in DB, FALSE otherwise]
def isVehiculeAlreadySaved(myConnexionToBDD, vehicle):

    try:
        cursor = myConnexionToBDD.cursor()

        cursor.execute("""SELECT * FROM siv WHERE immatriculation=?""", (vehicle['immatriculation'], ))

        if (cursor.fetchone() == None):
            return False
        else:
            return True

    except Exception as erreur:
        print("Erreur dans la verification de la présence du véhicule en base de données !")
        sys.exit(3)

# This function insert datas to the Database
# Parameter : [ myConnexionToBDD, vehicle ]
# Return : [ Nothing ]
def insertVehicleInDataBase(myConnexionToBDD, vehicle):

    try:
        cursor = myConnexionToBDD.cursor()

        listDataToInsert = [
                    vehicle['adresse_titulaire'], 
                    vehicle['nom'], 
                    vehicle['prenom'], 
                    vehicle['immatriculation'], 
                    vehicle['date_immatriculation'],
                    vehicle['vin'],
                    vehicle['marque'],
                    vehicle['denomination_commerciale'],
                    vehicle['couleur'],
                    vehicle['carrosserie'],
                    vehicle['categorie'],
                    vehicle['cylindree'],
                    vehicle['energie'],
                    vehicle['places'],
                    vehicle['poids'],
                    vehicle['puissance'],
                    vehicle['type'],
                    vehicle['variante'],
                    vehicle['version'] 
                ]

        cursor.execute("INSERT INTO siv VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", listDataToInsert)

    except Exception as erreur:
        print("Il y a eu une erreur dans l'insertion des données du véhicule !")
        sys.exit(4)

# This function update datas about a vehicule into the Database
# Parameter : [ myConnexionToBDD, vehicle ]
# Return : [ Nothing ]
def updateVehicleInDataBase(myConnexionToBDD, vehicle):

    try:
        cursor = myConnexionToBDD.cursor()

        listDataToUpdate = [
            vehicle['adresse_titulaire'], 
            vehicle['nom'], 
            vehicle['prenom'],
            vehicle['date_immatriculation'],
            vehicle['vin'],
            vehicle['marque'],
            vehicle['denomination_commerciale'],
            vehicle['couleur'],
            vehicle['carrosserie'],
            vehicle['categorie'],
            vehicle['cylindree'],
            vehicle['energie'],
            vehicle['places'],
            vehicle['poids'],
            vehicle['puissance'],
            vehicle['type'],
            vehicle['variante'],
            vehicle['version'],
            vehicle['immatriculation']
        ]

        cursor.execute(''' UPDATE siv SET 
                                        adress_titulaire = ?, nom = ?, prenom = ?, date_immatriculation = ?, vin = ?, marque = ?,
                                        denomination = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?,
                                        places = ?, poids = ?, puissance = ?, typeVehicule = ?, variante = ?, versionVehicule = ? WHERE immatriculation = ?''', listDataToUpdate)

    except Exception as erreur:
        print("Il y a eu une erreur dans la mise à jour des données !")
        sys.exit(5)

# This is the main function which uses all the others set before
# Parameter : [pathToCSV, pathToBDD]
# Return : [Nothing]
def importVehiclesDatas(pathToCSV, pathToBDD):

    try:

        compteurInsertion = 0
        compteurMiseAJour = 0

        nomTable = "siv"

        logging.basicConfig(filename='logs.log', format='%(asctime)s - %(message)s'  ,  level=logging.DEBUG)

        myConnectionToBDD = sqlite3.connect(pathToBDD)

        logging.debug("Connection a la base de donnees !")

        initTable(myConnectionToBDD)

        logging.debug("Creation de la table SIV (si inexistante)")

        logging.debug("Il y a %d données dans la table SIV" % rowCount(myConnectionToBDD))


        myListOfVehicles = setVehiclesList(pathToCSV)

        logging.debug("Initialisation des donnees a inserer !")

        for vehicle in  myListOfVehicles[1:]:

            if isVehiculeAlreadySaved(myConnectionToBDD, vehicle):
                print("Le véhicule immatriculé %s sera mis à jour !"% vehicle['immatriculation'])
                updateVehicleInDataBase(myConnectionToBDD, vehicle)
                compteurMiseAJour = compteurMiseAJour + 1
            else :
                print("Le véhicule immatriculé %s n'était pas connu, il sera inséré en BDD !" % vehicle['immatriculation'])
                insertVehicleInDataBase(myConnectionToBDD, vehicle)
                compteurInsertion = compteurInsertion + 1

        logging.debug("Il y a eu %d donnees mises a jour !" % compteurMiseAJour)
        logging.debug("Il y a eu %d nouvelles donnees inserees !" % compteurInsertion)
        logging.debug("Il y a %d donnees dans la table SIV" % rowCount(myConnectionToBDD))

        myConnectionToBDD.commit()
        myConnectionToBDD.close()

    except Exception as erreur:

        print("Erreur d'execution à l'execution du programme principal !")
        logging.warning(erreur)
        sys.exit(6)

# Compter le nombre de ligne et vérifier qu'il y ait bien 19 colonnes ?